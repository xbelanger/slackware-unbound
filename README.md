# Unbound Shell Scripts for Slackware

Here is a short collection of shell scripts to be used with Unbound on a GNU/Linux Slackware system.

 - https://nlnetlabs.nl/projects/unbound/about/
 - http://www.slackware.com/

## rc.unbound

This script should be copied in the /etc/rc.d/ folder and made executable. I will allow you to perform some basic operations on the Unbound daemon (stop/start/restart/...).

## insert-rc.unbound

This script adds a block in the /etc/rc.d/rc.inet2 script to start Unbound when the system boots.

## logrotate-unbound

This file can be copied to the /etc/logrotate.d folder and renamed 'unbound'. It contains the rules to rotate the Unbound log files.

## check-unbound-log

This script can be used to generate a basic report of Unbound's activity: number of queries by IP address, number of 'A' queries by hostname and number of queries by record type.

This require to use the _log-time-ascii: yes_ and _log-queries: yes_ directives in your configuration file.

